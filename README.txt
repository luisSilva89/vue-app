::::::::BACKEND:::::::::

Server side setup

-npm install nodemon eslint (nodemon will automatically restart our server just like webpack does it on the client side)
-npm install express (expressjs is a nodejs framework used to build RESTfull api's)
-npm install -g sequelize (to install sequelize)
-npm install -g sequelize-cli (sequelize command line interface)
-npm install -g pg (postgreSQL)
-npm install sequelize pg pg-hstore (A node package for serializing and deserializing JSON data to hstore format)
-npm install joi (framework used for validating http requests (body/query/parameters))
-npm install jsonwebtoken (for the authentication service)
-npm install bcrypt (package used to hash passwords)
-npm install bluebird (promise library)

Technologies being used

PostgreSQL: relational database
Expressjs: RESTfull API
Sequelize: Node.js ORM used to connect to databases
Node.js


::::::::FRONTEND::::::::

Client side setup

-npm install -g @vue/cli
-vue create <projectname> (creates a boilerplate template for a vuejs application)
-npm install (in project folder directory to add all dependencies we need)
-npm install axios (npm module library used to make http requests to the backend)
-npm install vuetify (UI framework, built with material design, on top of Vue.js used to stylize applications)
-npm install vuex --save (state management pattern + library for Vue.js applications)
-npm install vuex-router-sync (library to integrate vue-router with vuex)

Technologies being used

Vue.js: js framework implemented on Node.js
Vuetify: material design framework for vue.js

