--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3
-- Dumped by pg_dump version 13.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: AccessCodes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."AccessCodes" (
    id integer NOT NULL,
    code character varying(255) NOT NULL,
    "refPackage" integer NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."AccessCodes" OWNER TO postgres;

--
-- Name: AccessCodes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."AccessCodes_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AccessCodes_id_seq" OWNER TO postgres;

--
-- Name: AccessCodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."AccessCodes_id_seq" OWNED BY public."AccessCodes".id;


--
-- Name: Blueprints; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Blueprints" (
    id integer NOT NULL,
    url character varying(255) NOT NULL,
    "refGroup" integer NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."Blueprints" OWNER TO postgres;

--
-- Name: Blueprints_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Blueprints_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Blueprints_id_seq" OWNER TO postgres;

--
-- Name: Blueprints_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Blueprints_id_seq" OWNED BY public."Blueprints".id;


--
-- Name: Data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Data" (
    id integer NOT NULL,
    "refDeviceId" integer NOT NULL,
    eco2 real,
    tvoc real,
    red real,
    blue real,
    green real,
    lux real,
    light_temp_raw real,
    temp real,
    humidity real,
    r real,
    g real,
    b real,
    ir real,
    light_temp real,
    dbmax real,
    dbavg real,
    dbmin real,
    pressure real,
    ptemp real,
    "devicesCount" integer,
    "devicesFlow" integer,
    temperature real,
    ssid character varying(255),
    rssi integer,
    ssid_alt character varying(255),
    rssi_alt integer,
    "connectedToAlt" boolean,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."Data" OWNER TO postgres;

--
-- Name: Data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Data_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Data_id_seq" OWNER TO postgres;

--
-- Name: Data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Data_id_seq" OWNED BY public."Data".id;


--
-- Name: DevicePermissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."DevicePermissions" (
    id integer NOT NULL,
    "refDevice" integer NOT NULL,
    "refUser" integer NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."DevicePermissions" OWNER TO postgres;

--
-- Name: DevicePermissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."DevicePermissions_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."DevicePermissions_id_seq" OWNER TO postgres;

--
-- Name: DevicePermissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."DevicePermissions_id_seq" OWNED BY public."DevicePermissions".id;


--
-- Name: Devices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Devices" (
    id integer NOT NULL,
    name character varying(255),
    "serialNumber" character varying(255) NOT NULL,
    mac character varying(255) NOT NULL,
    "installationLocal" character varying(255),
    "refGroup" integer,
    "refAccessCode" integer,
    "rssiThreshold" integer,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."Devices" OWNER TO postgres;

--
-- Name: Devices_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Devices_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Devices_id_seq" OWNER TO postgres;

--
-- Name: Devices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Devices_id_seq" OWNED BY public."Devices".id;


--
-- Name: EventParameters; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."EventParameters" (
    id integer NOT NULL,
    "refDevice" integer NOT NULL,
    "refGroup" integer NOT NULL,
    "dataType" integer NOT NULL,
    condition integer NOT NULL,
    value float NOT NULL,
    "eventType" integer,
    message character varying(255),
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."EventParameters" OWNER TO postgres;

--
-- Name: EventParameters_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."EventParameters_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."EventParameters_id_seq" OWNER TO postgres;

--
-- Name: EventParameters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."EventParameters_id_seq" OWNED BY public."EventParameters".id;


--
-- Name: Events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Events" (
    id integer NOT NULL,
    "refDevice" integer NOT NULL,
    "refGroup" integer NOT NULL,
    "refData" integer NOT NULL,
    "dataType" integer NOT NULL,
    "eventType" integer,
    condition integer NOT NULL,
    value real NOT NULL,
    message character varying(255),
    read integer DEFAULT 0,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."Events" OWNER TO postgres;

--
-- Name: Events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Events_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Events_id_seq" OWNER TO postgres;

--
-- Name: Events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Events_id_seq" OWNED BY public."Events".id;


--
-- Name: Groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Groups" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    "refPackage" integer,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."Groups" OWNER TO postgres;

--
-- Name: Groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Groups_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Groups_id_seq" OWNER TO postgres;

--
-- Name: Groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Groups_id_seq" OWNED BY public."Groups".id;


--
-- Name: PackageServices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."PackageServices" (
    id integer NOT NULL,
    "refPackage" integer,
    "refService" integer,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."PackageServices" OWNER TO postgres;

--
-- Name: PackageServices_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."PackageServices_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PackageServices_id_seq" OWNER TO postgres;

--
-- Name: PackageServices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."PackageServices_id_seq" OWNED BY public."PackageServices".id;


--
-- Name: Packages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Packages" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    "cardsAvailable" integer NOT NULL,
    "sampleRate" integer NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."Packages" OWNER TO postgres;

--
-- Name: Packages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Packages_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Packages_id_seq" OWNER TO postgres;

--
-- Name: Packages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Packages_id_seq" OWNED BY public."Packages".id;


--
-- Name: PaidServices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."PaidServices" (
    id integer NOT NULL,
    "refGroup" integer NOT NULL,
    "refService" integer,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."PaidServices" OWNER TO postgres;

--
-- Name: PaidServices_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."PaidServices_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PaidServices_id_seq" OWNER TO postgres;

--
-- Name: PaidServices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."PaidServices_id_seq" OWNED BY public."PaidServices".id;


--
-- Name: Roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Roles" (
    id integer NOT NULL,
    value character varying(255) NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."Roles" OWNER TO postgres;

--
-- Name: Roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Roles_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Roles_id_seq" OWNER TO postgres;

--
-- Name: Roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Roles_id_seq" OWNED BY public."Roles".id;


--
-- Name: Services; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Services" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."Services" OWNER TO postgres;

--
-- Name: Services_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Services_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Services_id_seq" OWNER TO postgres;

--
-- Name: Services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Services_id_seq" OWNED BY public."Services".id;


--
-- Name: Users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Users" (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    username character varying(255),
    "firstName" character varying(255),
    "lastName" character varying(255),
    address character varying(255),
    "zipCode" character varying(255),
    nationality character varying(255),
    nif integer,
    "refRole" integer NOT NULL,
    "refGroup" integer NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now(),
    "updatedAt" timestamp with time zone DEFAULT now()
);


ALTER TABLE public."Users" OWNER TO postgres;

--
-- Name: Users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Users_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Users_id_seq" OWNER TO postgres;

--
-- Name: Users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Users_id_seq" OWNED BY public."Users".id;


--
-- Name: AccessCodes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."AccessCodes" ALTER COLUMN id SET DEFAULT nextval('public."AccessCodes_id_seq"'::regclass);


--
-- Name: Blueprints id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Blueprints" ALTER COLUMN id SET DEFAULT nextval('public."Blueprints_id_seq"'::regclass);


--
-- Name: Data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Data" ALTER COLUMN id SET DEFAULT nextval('public."Data_id_seq"'::regclass);


--
-- Name: DevicePermissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."DevicePermissions" ALTER COLUMN id SET DEFAULT nextval('public."DevicePermissions_id_seq"'::regclass);


--
-- Name: Devices id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Devices" ALTER COLUMN id SET DEFAULT nextval('public."Devices_id_seq"'::regclass);


--
-- Name: EventParameters id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EventParameters" ALTER COLUMN id SET DEFAULT nextval('public."EventParameters_id_seq"'::regclass);


--
-- Name: Events id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Events" ALTER COLUMN id SET DEFAULT nextval('public."Events_id_seq"'::regclass);


--
-- Name: Groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Groups" ALTER COLUMN id SET DEFAULT nextval('public."Groups_id_seq"'::regclass);


--
-- Name: PackageServices id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PackageServices" ALTER COLUMN id SET DEFAULT nextval('public."PackageServices_id_seq"'::regclass);


--
-- Name: Packages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Packages" ALTER COLUMN id SET DEFAULT nextval('public."Packages_id_seq"'::regclass);


--
-- Name: PaidServices id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PaidServices" ALTER COLUMN id SET DEFAULT nextval('public."PaidServices_id_seq"'::regclass);


--
-- Name: Roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Roles" ALTER COLUMN id SET DEFAULT nextval('public."Roles_id_seq"'::regclass);


--
-- Name: Services id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Services" ALTER COLUMN id SET DEFAULT nextval('public."Services_id_seq"'::regclass);


--
-- Name: Users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Users" ALTER COLUMN id SET DEFAULT nextval('public."Users_id_seq"'::regclass);


--
-- Data for Name: AccessCodes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."AccessCodes" (id, code, "refPackage", "createdAt", "updatedAt") FROM stdin;
1	1	3	2021-07-15 16:36:02.086447+01	2021-07-15 16:36:02.086447+01
\.


--
-- Data for Name: Blueprints; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Blueprints" (id, url, "refGroup", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: Data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Data" (id, "refDeviceId", eco2, tvoc, red, blue, green, lux, light_temp_raw, temp, humidity, r, g, b, ir, light_temp, dbmax, dbavg, dbmin, pressure, ptemp, "devicesCount", "devicesFlow", temperature, ssid, rssi, ssid_alt, rssi_alt, "connectedToAlt", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: DevicePermissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."DevicePermissions" (id, "refDevice", "refUser", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: Devices; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Devices" (id, name, "serialNumber", mac, "installationLocal", "refGroup", "refAccessCode", "rssiThreshold", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: EventParameters; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."EventParameters" (id, "refDevice", "refGroup", "dataType", condition, value, "eventType", message, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: Events; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Events" (id, "refDevice", "refGroup", "refData", "dataType", "eventType", condition, value, message, read, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: Groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Groups" (id, name, "refPackage", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: PackageServices; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."PackageServices" (id, "refPackage", "refService", "createdAt", "updatedAt") FROM stdin;
1	3	7	2021-07-15 16:32:24.416278+01	2021-07-15 16:32:24.416278+01
2	4	7	2021-07-15 16:32:27.67217+01	2021-07-15 16:32:27.67217+01
3	4	5	2021-07-15 16:32:30.850432+01	2021-07-15 16:32:30.850432+01
4	4	6	2021-07-15 16:32:33.142641+01	2021-07-15 16:32:33.142641+01
5	5	7	2021-07-15 16:32:38.318719+01	2021-07-15 16:32:38.318719+01
6	5	6	2021-07-15 16:32:48.698126+01	2021-07-15 16:32:48.698126+01
7	5	5	2021-07-15 16:32:50.857468+01	2021-07-15 16:32:50.857468+01
8	6	7	2021-07-15 16:32:58.208407+01	2021-07-15 16:32:58.208407+01
9	6	6	2021-07-15 16:32:59.812349+01	2021-07-15 16:32:59.812349+01
10	6	5	2021-07-15 16:33:00.914988+01	2021-07-15 16:33:00.914988+01
11	1	9	2021-07-15 16:33:06.531579+01	2021-07-15 16:33:06.531579+01
12	1	7	2021-07-15 16:33:08.121966+01	2021-07-15 16:33:08.121966+01
13	2	9	2021-07-15 16:33:13.880411+01	2021-07-15 16:33:13.880411+01
14	2	7	2021-07-15 16:33:15.482783+01	2021-07-15 16:33:15.482783+01
15	1	11	2021-07-15 16:33:21.71382+01	2021-07-15 16:33:21.71382+01
16	2	12	2021-07-15 16:33:26.852641+01	2021-07-15 16:33:26.852641+01
17	3	10	2021-07-15 16:33:34.241464+01	2021-07-15 16:33:34.241464+01
18	3	13	2021-07-15 16:33:36.640969+01	2021-07-15 16:33:36.640969+01
19	4	10	2021-07-15 16:33:43.090678+01	2021-07-15 16:33:43.090678+01
20	4	13	2021-07-15 16:33:44.50042+01	2021-07-15 16:33:44.50042+01
21	5	10	2021-07-15 16:33:50.103758+01	2021-07-15 16:33:50.103758+01
22	5	13	2021-07-15 16:33:51.569781+01	2021-07-15 16:33:51.569781+01
23	6	10	2021-07-15 16:33:55.901066+01	2021-07-15 16:33:55.901066+01
24	6	13	2021-07-15 16:33:57.056692+01	2021-07-15 16:33:57.056692+01
\.


--
-- Data for Name: Packages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Packages" (id, name, "cardsAvailable", "sampleRate", "createdAt", "updatedAt") FROM stdin;
1	individual	8	20	2021-07-15 16:29:11.448656+01	2021-07-15 16:29:11.448656+01
2	multiple	8	10	2021-07-15 16:29:23.060232+01	2021-07-15 16:29:23.060232+01
3	minius	12	5	2021-07-15 16:29:31.656443+01	2021-07-15 16:29:31.656443+01
4	limia	12	5	2021-07-15 16:29:38.222074+01	2021-07-15 16:29:38.222074+01
5	nerbis	12	5	2021-07-15 16:29:44.759039+01	2021-07-15 16:29:44.759039+01
6	turia	12	5	2021-07-15 16:29:49.382043+01	2021-07-15 16:29:49.382043+01
\.


--
-- Data for Name: PaidServices; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."PaidServices" (id, "refGroup", "refService", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: Roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Roles" (id, value, "createdAt", "updatedAt") FROM stdin;
1	admin	2021-07-15 16:34:39.368231+01	2021-07-15 16:34:39.368231+01
2	user	2021-07-15 16:34:44.126612+01	2021-07-15 16:34:44.126612+01
3	master	2021-07-15 16:34:52.345054+01	2021-07-15 16:34:52.345054+01
\.


--
-- Data for Name: Services; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Services" (id, name, "createdAt", "updatedAt") FROM stdin;
1	csvExport	2021-07-15 16:30:36.957271+01	2021-07-15 16:30:36.957271+01
2	extIntegration	2021-07-15 16:30:44.619284+01	2021-07-15 16:30:44.619284+01
3	notification	2021-07-15 16:30:51.170333+01	2021-07-15 16:30:51.170333+01
4	advPortal	2021-07-15 16:30:58.520494+01	2021-07-15 16:30:58.520494+01
5	analytics	2021-07-15 16:31:04.986277+01	2021-07-15 16:31:04.986277+01
6	control	2021-07-15 16:31:08.09195+01	2021-07-15 16:31:08.09195+01
7	limitedPortal	2021-07-15 16:31:12.775075+01	2021-07-15 16:31:12.775075+01
8	feeBasic	2021-07-15 16:31:17.725038+01	2021-07-15 16:31:17.725038+01
9	8cards	2021-07-15 16:31:22.522728+01	2021-07-15 16:31:22.522728+01
10	12cards	2021-07-15 16:31:26.770066+01	2021-07-15 16:31:26.770066+01
11	sr20	2021-07-15 16:31:32.064025+01	2021-07-15 16:31:32.064025+01
12	sr10	2021-07-15 16:31:33.472103+01	2021-07-15 16:31:33.472103+01
13	sr5	2021-07-15 16:31:38.005285+01	2021-07-15 16:31:38.005285+01
\.


--
-- Data for Name: Users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Users" (id, email, password, username, "firstName", "lastName", address, "zipCode", nationality, nif, "refRole", "refGroup", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: AccessCodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."AccessCodes_id_seq"', 1, true);


--
-- Name: Blueprints_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Blueprints_id_seq"', 1, false);


--
-- Name: Data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Data_id_seq"', 1, false);


--
-- Name: DevicePermissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."DevicePermissions_id_seq"', 1, false);


--
-- Name: Devices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Devices_id_seq"', 1, false);


--
-- Name: EventParameters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."EventParameters_id_seq"', 1, false);


--
-- Name: Events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Events_id_seq"', 1, false);


--
-- Name: Groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Groups_id_seq"', 1, false);


--
-- Name: PackageServices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."PackageServices_id_seq"', 24, true);


--
-- Name: Packages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Packages_id_seq"', 6, true);


--
-- Name: PaidServices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."PaidServices_id_seq"', 1, false);


--
-- Name: Roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Roles_id_seq"', 3, true);


--
-- Name: Services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Services_id_seq"', 13, true);


--
-- Name: Users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Users_id_seq"', 1, false);


--
-- Name: AccessCodes AccessCodes_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."AccessCodes"
    ADD CONSTRAINT "AccessCodes_code_key" UNIQUE (code);


--
-- Name: AccessCodes AccessCodes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."AccessCodes"
    ADD CONSTRAINT "AccessCodes_pkey" PRIMARY KEY (id);


--
-- Name: Blueprints Blueprints_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Blueprints"
    ADD CONSTRAINT "Blueprints_pkey" PRIMARY KEY (id);


--
-- Name: Data Data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Data"
    ADD CONSTRAINT "Data_pkey" PRIMARY KEY (id);


--
-- Name: DevicePermissions DevicePermissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."DevicePermissions"
    ADD CONSTRAINT "DevicePermissions_pkey" PRIMARY KEY (id);


--
-- Name: Devices Devices_mac_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Devices"
    ADD CONSTRAINT "Devices_mac_key" UNIQUE (mac);


--
-- Name: Devices Devices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Devices"
    ADD CONSTRAINT "Devices_pkey" PRIMARY KEY (id);


--
-- Name: Devices Devices_serialNumber_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Devices"
    ADD CONSTRAINT "Devices_serialNumber_key" UNIQUE ("serialNumber");


--
-- Name: EventParameters EventParameters_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EventParameters"
    ADD CONSTRAINT "EventParameters_pkey" PRIMARY KEY (id);


--
-- Name: Events Events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Events"
    ADD CONSTRAINT "Events_pkey" PRIMARY KEY (id);


--
-- Name: Groups Groups_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Groups"
    ADD CONSTRAINT "Groups_name_key" UNIQUE (name);


--
-- Name: Groups Groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Groups"
    ADD CONSTRAINT "Groups_pkey" PRIMARY KEY (id);


--
-- Name: PackageServices PackageServices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PackageServices"
    ADD CONSTRAINT "PackageServices_pkey" PRIMARY KEY (id);


--
-- Name: Packages Packages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Packages"
    ADD CONSTRAINT "Packages_pkey" PRIMARY KEY (id);


--
-- Name: PaidServices PaidServices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PaidServices"
    ADD CONSTRAINT "PaidServices_pkey" PRIMARY KEY (id);


--
-- Name: Roles Roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Roles"
    ADD CONSTRAINT "Roles_pkey" PRIMARY KEY (id);


--
-- Name: Roles Roles_value_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Roles"
    ADD CONSTRAINT "Roles_value_key" UNIQUE (value);


--
-- Name: Services Services_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Services"
    ADD CONSTRAINT "Services_pkey" PRIMARY KEY (id);


--
-- Name: Users Users_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_email_key" UNIQUE (email);


--
-- Name: Users Users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (id);


--
-- Name: AccessCodes AccessCodes_refPackage_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."AccessCodes"
    ADD CONSTRAINT "AccessCodes_refPackage_fkey" FOREIGN KEY ("refPackage") REFERENCES public."Packages"(id) ON UPDATE CASCADE;


--
-- Name: Blueprints Blueprints_refGroup_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Blueprints"
    ADD CONSTRAINT "Blueprints_refGroup_fkey" FOREIGN KEY ("refGroup") REFERENCES public."Groups"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Data Data_refDeviceId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Data"
    ADD CONSTRAINT "Data_refDeviceId_fkey" FOREIGN KEY ("refDeviceId") REFERENCES public."Devices"(id) ON UPDATE CASCADE;


--
-- Name: DevicePermissions DevicePermissions_refDevice_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."DevicePermissions"
    ADD CONSTRAINT "DevicePermissions_refDevice_fkey" FOREIGN KEY ("refDevice") REFERENCES public."Devices"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: DevicePermissions DevicePermissions_refUser_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."DevicePermissions"
    ADD CONSTRAINT "DevicePermissions_refUser_fkey" FOREIGN KEY ("refUser") REFERENCES public."Users"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Devices Devices_refAccessCode_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Devices"
    ADD CONSTRAINT "Devices_refAccessCode_fkey" FOREIGN KEY ("refAccessCode") REFERENCES public."AccessCodes"(id) ON UPDATE CASCADE;


--
-- Name: Devices Devices_refGroup_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Devices"
    ADD CONSTRAINT "Devices_refGroup_fkey" FOREIGN KEY ("refGroup") REFERENCES public."Groups"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: EventParameters EventParameters_refDevice_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EventParameters"
    ADD CONSTRAINT "EventParameters_refDevice_fkey" FOREIGN KEY ("refDevice") REFERENCES public."Devices"(id) ON UPDATE CASCADE;


--
-- Name: EventParameters EventParameters_refGroup_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EventParameters"
    ADD CONSTRAINT "EventParameters_refGroup_fkey" FOREIGN KEY ("refGroup") REFERENCES public."Groups"(id) ON UPDATE CASCADE;


--
-- Name: Events Events_refData_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Events"
    ADD CONSTRAINT "Events_refData_fkey" FOREIGN KEY ("refData") REFERENCES public."Data"(id) ON UPDATE CASCADE;


--
-- Name: Events Events_refDevice_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Events"
    ADD CONSTRAINT "Events_refDevice_fkey" FOREIGN KEY ("refDevice") REFERENCES public."Devices"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Events Events_refGroup_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Events"
    ADD CONSTRAINT "Events_refGroup_fkey" FOREIGN KEY ("refGroup") REFERENCES public."Groups"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Groups Groups_refPackage_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Groups"
    ADD CONSTRAINT "Groups_refPackage_fkey" FOREIGN KEY ("refPackage") REFERENCES public."Packages"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: PackageServices PackageServices_refPackage_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PackageServices"
    ADD CONSTRAINT "PackageServices_refPackage_fkey" FOREIGN KEY ("refPackage") REFERENCES public."Packages"(id);


--
-- Name: PackageServices PackageServices_refService_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PackageServices"
    ADD CONSTRAINT "PackageServices_refService_fkey" FOREIGN KEY ("refService") REFERENCES public."Services"(id) ON UPDATE CASCADE;


--
-- Name: PaidServices PaidServices_refGroup_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PaidServices"
    ADD CONSTRAINT "PaidServices_refGroup_fkey" FOREIGN KEY ("refGroup") REFERENCES public."Groups"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: PaidServices PaidServices_refService_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PaidServices"
    ADD CONSTRAINT "PaidServices_refService_fkey" FOREIGN KEY ("refService") REFERENCES public."Services"(id) ON UPDATE CASCADE;


--
-- Name: Users Users_refGroup_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_refGroup_fkey" FOREIGN KEY ("refGroup") REFERENCES public."Groups"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Users Users_refRole_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_refRole_fkey" FOREIGN KEY ("refRole") REFERENCES public."Roles"(id) ON UPDATE CASCADE;


--
-- PostgreSQL database dump complete
--

