require('dotenv').config()

module.exports = {
  port: process.env.PORT || 8081,
  // https: true,
  db: {
    database: process.env.DB_NAME || 'humanexpdb',
    user: process.env.DB_USER || 'postgres',
    password: process.env.DB_PASS || '&HExp@2021$&',
    options: {
      dialect: process.env.DIALECT || 'postgres',
      host: process.env.HOST || '94.46.178.132',
      timezone: "UTC",
      additional: {
        timestamps: true
      }
    }
  },
  authentication: {
    jwtSecret: process.env.ACCESS_TOKEN_SECRET
  }
}
