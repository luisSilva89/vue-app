module.exports = (sequelize, DataTypes) => {
  const Data = sequelize.define('Data',
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    refDeviceId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
				model: 'Devices',
				key: 'id'
			}
    },

    //CO2
    eco2: {
      type: DataTypes.REAL
    },
    tvoc: {
      type: DataTypes.REAL
    },

    //RGB & LIGHT
    red: {
      type: DataTypes.REAL
    },
    blue: {
      type: DataTypes.REAL
    },
    green: {
      type: DataTypes.REAL
    },
    lux: {
      type: DataTypes.REAL
    },
    light_temp_raw: {
      type: DataTypes.REAL
    },

    //HUMIDITY
    temp: {
      type: DataTypes.REAL
    },
    humidity: {
      type: DataTypes.REAL
    },

    //INFRARED
    r: {
      type: DataTypes.REAL
    },
    g: {
      type: DataTypes.REAL
    },
    b: {
      type: DataTypes.REAL
    },
    ir: {
      type: DataTypes.REAL
    },
    light_temp: {
      type: DataTypes.REAL
    },

    //NOISE
    dbmax: {
      type: DataTypes.REAL
    },
    dbavg: {
      type: DataTypes.REAL
    },
    dbmin: {
      type: DataTypes.REAL
    },

    //PRESSURE
    pressure: {
      type: DataTypes.REAL
    },
    ptemp: {
      type: DataTypes.REAL
    },

    //SNIFFER
    devicesCount: {
        type: DataTypes.INTEGER(2).UNSIGNED
    },
    devicesFlow: {
      type: DataTypes.INTEGER(2).UNSIGNED
  },

    //TEMPERATURE
    temperature: {
      type: DataTypes.REAL
    },

    //WIFI
    ssid: {
      type: DataTypes.STRING
    },
    rssi: {
      type: DataTypes.INTEGER(2)
    },
    ssid_alt: {
      type: DataTypes.STRING
    },
    rssi_alt: {
      type: DataTypes.INTEGER(2)
    },
    connectedToAlt: {
     type: DataTypes.BOOLEAN
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('NOW()')
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('NOW()')
    }
  },
  {
    freezeTableName: true
  })

  Data.associate = function (models) {

    Data.belongsTo(models.Device, {
      foreignKey: 'refDeviceId'
    })
  }

  return Data
}
