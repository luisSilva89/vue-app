module.exports = (sequelize, DataTypes) => {
  const DevicePermission = sequelize.define("DevicePermission", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    refDevice: {
      type: DataTypes.INTEGER,
      references: {
        model: "Devices",
        key: "id",
      },
    },
    refUser: {
      type: DataTypes.INTEGER,
      references: {
        model: "Users",
        key: "id",
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
  });

  DevicePermission.associate = function (models) {
    DevicePermission.belongsTo(models.Device, {
      foreignKey: { allowNull: false, name: "refDevice" },
      onDelete: "cascade",
      hooks: true,
    });
    DevicePermission.belongsTo(models.User, {
      foreignKey: { allowNull: false, name: "refUser" },
      onDelete: "cascade",
      hooks: true,
    });
  };

  return DevicePermission;
};
