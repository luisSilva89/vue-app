module.exports = (sequelize, DataTypes) => {
  const Event = sequelize.define("Event", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    refDevice: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Devices",
        key: "id",
      },
    },
    refGroup: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Groups",
        key: "id",
      },
    },
    refData: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Data",
        key: "id",
      },
    },
    dataType: {
      type: DataTypes.INTEGER(2).UNSIGNED,
      allowNull: false,
    },
    eventType: {
      type: DataTypes.INTEGER(1).UNSIGNED,
    },
    condition: {
      type: DataTypes.INTEGER(1).UNSIGNED,
      allowNull: false,
    },
    value: {
      type: DataTypes.REAL,
      allowNull: false,
    },
    message: {
      type: DataTypes.STRING,
    },
    read: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
  });

  Event.associate = function (models) {
    Event.belongsTo(models.Group, {
      foreignKey: { allowNull: false, name: "refGroup" },
      onDelete: "cascade",
      hooks: true,
    });
    Event.belongsTo(models.Device, {
      foreignKey: { allowNull: false, name: "refDevice" },
      onDelete: "cascade",
      hooks: true,
    });
    Event.belongsTo(models.Data, {
      foreignKey: "refData",
    });
  };

  return Event;
};
