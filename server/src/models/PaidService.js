module.exports = (sequelize, DataTypes) => {
  const PaidService = sequelize.define("PaidService", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    refGroup: {
      type: DataTypes.INTEGER,
      references: {
        model: "Groups",
        key: "id",
      },
    },
    refService: {
      type: DataTypes.INTEGER,
      references: {
        model: "Services",
        key: "id",
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
  });

  PaidService.associate = function (models) {
    PaidService.belongsTo(models.Group, {
      foreignKey: { name: "refGroup", allowNull: false },
      onDelete: "cascade",
      hooks: true,
    });
    PaidService.belongsTo(models.Service, {
      foreignKey: "refService",
    });
  };

  return PaidService;
};
