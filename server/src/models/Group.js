module.exports = (sequelize, DataTypes) => {
  const Group = sequelize.define("Group", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    refPackage: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: "Package",
        key: "id",
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
  });

  Group.associate = function (models) {
    Group.belongsTo(models.Package, {
      foreignKey: "refPackage",
    });
  };

  return Group;
};
