module.exports = (sequelize, DataTypes) => {
  const PackageService = sequelize.define("PackageService", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    refPackage: {
      type: DataTypes.INTEGER,
      references: {
        model: "Packages",
        key: "id",
      },
    },
    refService: {
      type: DataTypes.INTEGER,
      references: {
        model: "Services",
        key: "id",
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
  });

  PackageService.associate = function (models) {
    PackageService.belongsTo(models.Package, {
      foreignKey: "refPackage",
    });
  };
  PackageService.associate = function (models) {
    PackageService.belongsTo(models.Service, {
      foreignKey: "refService",
    });
  };

  return PackageService;
};
