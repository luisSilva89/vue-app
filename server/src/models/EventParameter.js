module.exports = (sequelize, DataTypes) => {
  const EventParameter = sequelize.define("EventParameter", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    refDevice: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Devices",
        key: "id",
      },
    },
    refGroup: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Groups",
        key: "id",
      },
    },
    dataType: {
      type: DataTypes.INTEGER(2).UNSIGNED,
      allowNull: false,
    },
    condition: {
      type: DataTypes.INTEGER(1).UNSIGNED,
      allowNull: false,
    },
    value: {
      type: DataTypes.REAL,
      allowNull: false,
    },
    eventType: {
      type: DataTypes.INTEGER(1).UNSIGNED,
    },
    message: {
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
  });

  EventParameter.associate = function (models) {
    EventParameter.belongsTo(models.Group, {
      foreignKey: "refGroup",
    });
    EventParameter.belongsTo(models.Device, {
      foreignKey: "refDevice",
    });
  };

  return EventParameter;
};
