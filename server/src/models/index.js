// fs module is how node.js reads through the file system
const fs = require('fs')
// path module is how node.js deals with absolute and relative paths 
const path = require('path')
// Sequelize module is how we create a sequelize object and connect it to the db
const Sequelize = require('sequelize')
const config = require('../config/config')
db = {}

const sequelize = new Sequelize(
  config.db.database,
  config.db.user,
  config.db.password,
  config.db.options,
)

// functionality to automatically read through all models and set them to be used by sequelize
fs
  // reads the current directory
  .readdirSync(__dirname)
  // gives us an array of different files and filters out the file index.js
  .filter((file) => {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  // for each file found sequelize will import it and set it to be used
  // "join" joins the directory name with the file name
  .forEach((file) => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes)
    db[model.name] = model
  })

  Object.keys(db).forEach(function (modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});

// when we use variables sequelize and Sequelize we have access to the db objects
db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db