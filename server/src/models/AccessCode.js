module.exports = (sequelize, DataTypes) => {
  const AccessCode = sequelize.define("AccessCode", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    code: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    refPackage: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Packages",
        key: "id",
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
  });

  AccessCode.associate = function (models) {
    AccessCode.belongsTo(models.Package, {
      foreignKey: "refPackage",
    });
  };

  return AccessCode;
};
