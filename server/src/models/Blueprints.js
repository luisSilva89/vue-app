module.exports = (sequelize, DataTypes) => {
  const Blueprint = sequelize.define("Blueprint", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    refGroup: {
      type: DataTypes.INTEGER,
      references: {
        model: "Groups",
        key: "id",
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
  });

  Blueprint.associate = function (models) {
    Blueprint.belongsTo(models.Group, {
      foreignKey: { name: "refGroup", allowNull: false },
      onDelete: "cascade",
      hooks: true,
    });
  };

  return Blueprint;
};
