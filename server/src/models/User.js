const bcrypt = require("bcrypt");

function hashPassword(user) {
  // salt adds bits to the password to create unique hashed passwords
  const SALT_FACTOR = 8;
  user.password = bcrypt.hashSync(
    user.password,
    bcrypt.genSaltSync(SALT_FACTOR)
  );
}

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      username: DataTypes.STRING,
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      address: DataTypes.STRING,
      zipCode: DataTypes.STRING,
      nationality: DataTypes.STRING,
      nif: DataTypes.INTEGER,
      refRole: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Roles",
          key: "id",
        },
      },
      refGroup: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Groups",
          key: "id",
        },
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.literal("NOW()"),
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.literal("NOW()"),
      },
    },
    {
      hooks: {
        beforeCreate: hashPassword,
        // beforeUpdate: hashPassword,
      },
    }
  );

  User.associate = function (models) {
    User.belongsTo(models.Role, {
      foreignKey: "refRole",
    });
    User.belongsTo(models.Group, {
      foreignKey: { name: "refGroup", allowNull: false },
      onDelete: "cascade",
      hooks: true,
    });
  };

  // compare the user login password with the encrypted password stored on the model
  User.prototype.comparePassword = function (password) {
    return bcrypt.compareSync(password, this.password);
  };

  return User;
};
