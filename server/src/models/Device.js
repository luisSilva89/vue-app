module.exports = (sequelize, DataTypes) => {
  const Device = sequelize.define("Device", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
    },
    serialNumber: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    // TODO CHANGE DATA TYPE TO INTEGER BIGINT
    mac: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    installationLocal: {
      type: DataTypes.STRING,
    },
    refGroup: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: "Groups",
        key: "id",
      },
    },
    refPackage: {
      type: DataTypes.INTEGER,
      references: {
        model: "Package",
        key: "id",
      },
    },
    rssiThreshold: {
      type: DataTypes.INTEGER,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("NOW()"),
    },
  });

  Device.associate = function (models) {
    Device.belongsTo(models.Package, {
      foreignKey: "refPackage",
    });
    Device.belongsTo(models.Group, {
      foreignKey: { name: "refGroup", allowNull: false },
      onDelete: "cascade",
      hooks: true,
    });
  };

  return Device;
};
