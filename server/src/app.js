const express = require('express')

// cors allows any host or client access
const cors = require('cors')

// use the log generator morgan to personalize commandline logs
const morgan = require('morgan')
//const bodyParser = require('body-parser')
const {sequelize} = require('./models')
const config = require('./config/config')

// builds an express server
const app = express()

app.use(morgan('combined'))
app.use(express.json({limit: '25mb', extended: true}));
app.use(express.urlencoded({limit: '25mb', extended: true}));
app.use(cors())

require('./passport')

require('./routes')(app)

// pass {force: true} as an argument to the sequelize.sync method to clear the table on the db 
sequelize.sync()
  .then(() => {
    app.listen(config.port)
    console.log(`Server started on port ${config.port}`)
  })

