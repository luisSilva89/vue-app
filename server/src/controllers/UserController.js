const { User } = require("../models");

module.exports = {
  async getUserInfo(req, res) {
    let userId = req.query.user;
    let options = {
      where: {
        id: userId,
      },
    };
    try {
      let user = await User.findOne(options);
      res.send(user);
    } catch (err) {
      console.log(err);
    }
  },

  async updateUserInfo(req, res) {
    let userId = req.user.id;
    let options = {
      where: {
        id: userId,
      },
    };
    try {
      let user = await User.findOne(options);
      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.address = req.body.address;
      user.zipCode = req.body.zipCode;
      user.nationality = req.body.nationality;
      user.nif = req.body.nif;
      await user.save();

      let userJson = user.toJSON();
      res.send({
        user: userJson,
      });
    } catch (err) {
      res.status(500).send({
        error: "An error occurred while updating user info.",
      });
      console.log('ERROR:::::', err);
    }
  },

  async createUser(req, res) {
    let refRole = "";

    switch (req.body.refRole) {
      case "admin":
        refRole = 1;
        break;
      case "user":
        refRole = 2;
        break;
      case "master":
        refRole = 3;
        break;
      default:
        refRole = null;
    }

    try {
      const user = await User.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        refRole: refRole,
        refGroup: req.body.refGroup,
      });

      const userJson = user.toJSON();
      res.send({
        user: userJson,
      });
    } catch (err) {
      console.log("ERROR: ", err);
      res.status(400).send({
        error: "This email account is already in use",
      });
    }
  },

  async getAllUsers(req, res) {
    try {
      let users = await User.findAll();
      res.send(users);
    } catch (err) {
      console.log(err)
    }
  },

  async delete(req, res) {
    const id = Number(req.params.id);

    let options = {
      where: {
        id: id,
      },
    };
    try {
      await User.destroy(options);
      res.status(200).send();
    } catch (err) {
      console.log(err);
    }
  },
};
