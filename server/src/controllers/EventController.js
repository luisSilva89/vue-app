const { EventParameter, Event } = require("../models");

module.exports = {
  async createNewEntry(req, res) {
    try {
      const entry = await EventParameter.create({
        refDevice: req.body.refDevice,
        refGroup: req.body.refGroup,
        dataType: req.body.dataType,
        condition: req.body.condition,
        value: req.body.value,
      });

      const entryJson = entry.toJSON();
      res.send({
        entry: entryJson,
      });
    } catch (err) {
      console.log("ERROR: ", err);
      res.status(400).send({
        error: "An error occurred while creating entry.",
      });
    }
  },

  async getEntries(req, res) {
    const groupId = Number(req.query.groupId);
    options = {
      where: {
        refGroup: groupId,
      },
    };
    try {
      await EventParameter.findAll(options).then((entries) => {
        res.send(entries);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async updateAction(req, res) {
    let options = {
      where: {
        id: req.body.id,
      },
    };
    try {
      let entry = await EventParameter.findOne(options);
      entry.eventType = req.body.eventType;
      entry.message = req.body.message;
      await entry.save();

      let entryJson = entry.toJSON();
      res.send({
        entry: entryJson,
      });
    } catch (err) {
      res.status(500).send({
        error: "An error occurred while updating user info.",
      });
      console.log("ERROR:", err);
    }
  },

  async getCurrentEvents(req, res) {
    const dataId = Number(req.query.dataId);
    const deviceId = Number(req.query.deviceId);

    let options = {
      where: {
        refData: dataId,
        refDevice: deviceId,
      },
    };
    try {
      let response = await Event.findAll(options);
      res.send(response);
    } catch (err) {
      console.log("ERROR: ", err);
    }
  },

  async getEvents(req, res) {
    const groupId = Number(req.query.groupId);

    let options = {
      where: {
        refGroup: groupId,
      },
      order: [["createdAt", "DESC"]],
    };
    try {
      let response = await Event.findAll(options);
      res.send(response);
    } catch (err) {
      console.log(err);
    }
  },

  async delete(req, res) {
    const id = Number(req.params.id);

    let options = {
      where: {
        id: id,
      },
    };
    try {
      await EventParameter.destroy(options);
      res.status(200).send();
    } catch (err) {
      console.log(err);
    }
  },
};
