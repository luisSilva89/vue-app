const { Data } = require("../models");
const { Op, literal } = require("sequelize");

module.exports = {
  //Method that fetches the last inserted data on the data table. Use it to get only the latest sensor values.
  async getCurrentData(req, res) {
    const deviceId = Number(req.body.params.device[0]);

    let options = {
      order: [["createdAt", "DESC"]],
      limit: 1,
      where: {
        refDeviceId: deviceId,
      },
    };
    try {
      let payload = {};

      let payloadResponse = await Data.findAll(options);
      payload.data = payloadResponse[0].dataValues;
      res.send(payload);
    } catch (err) {
      console.log(err);
    }
  },

  //Method to fetch temperature (by default) values per given timeframe in order to
  //populate the lobby graph with a default setup. Default datatype can be changed in
  //settings although currently it is hardcoded in this method.
  async getDefaultPlotData(req, res) {
    const deviceId = Number(req.query.deviceId);
    const dateFrom = req.query.dateFrom;
    const dateTo = req.query.dateTo;
    console.log("dateFrom: ", dateFrom + "  dateTo:", dateTo);
    let options = {
      attributes: ["temperature", "createdAt"],
      where: {
        refDeviceId: deviceId,
        createdAt: {
          [Op.gte]: dateFrom,
          [Op.lte]: dateTo,
        },
      },
      order: [["createdAt", "ASC"]],
    };
    try {
      Data.findAll(options).then((tempByInterval) => {
        res.send(tempByInterval);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getDataFromAllDevices(req, res) {
    try {
      const deviceIds = req.query.deviceIds;
      const dataType = req.query.dataType;
      const options = {
        attributes: [
          literal('DISTINCT ON ("refDeviceId") "refDeviceId"'),
          "refDeviceId",
          `${dataType}`,
          "createdAt",
        ],
        order: [
          ["refDeviceId", "ASC"],
          ["createdAt", "DESC"],
        ],
        where: {
          refDeviceId: {
            [Op.in]: deviceIds,
          },
        },
      };

      const response = await Data.findAll(options);
      res.send(response);
    } catch (err) {
      console.log(err);
    }
  },

  //Method to fetch data from all sensors from a pre determined timeframe
  async getDataByInterval(req, res) {
    let deviceId = req.query.selectedDevices;
    const dateFrom = req.query.dateFrom;
    const dateTo = req.query.dateTo;

    let options = {
      where: {
        refDeviceId: deviceId,
        createdAt: {
          [Op.gte]: dateFrom,
          [Op.lte]: dateTo,
        },
      },
      order: [["createdAt", "DESC"]],
    };
    try {
      Data.findAll(options).then((dataByInterval) => {
        res.send(dataByInterval);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getTempByInterval(req, res) {
    let deviceId = req.query.selectedDevices;
    const dateFrom = req.query.dateFrom;
    const dateTo = req.query.dateTo;

    let options = {
      attributes: ["temperature", "createdAt"],
      where: {
        refDeviceId: deviceId,
        createdAt: {
          [Op.gte]: dateFrom,
          [Op.lte]: dateTo,
        },
      },
      order: [["createdAt", "ASC"]],
    };
    try {
      Data.findAll(options).then((tempByInterval) => {
        res.send(tempByInterval);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getNoiseByInterval(req, res) {
    const deviceId = req.query.selectedDevices;
    const dateFrom = req.query.dateFrom;
    const dateTo = req.query.dateTo;
    let options = {
      attributes: ["dbavg", "createdAt"],
      where: {
        refDeviceId: deviceId,
        createdAt: {
          [Op.gte]: dateFrom,
          [Op.lte]: dateTo,
        },
      },
      order: [["createdAt", "ASC"]],
    };
    try {
      Data.findAll(options).then((noiseByInterval) => {
        res.send(noiseByInterval);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getHumByInterval(req, res) {
    const deviceId = req.query.selectedDevices;
    const dateFrom = req.query.dateFrom;
    const dateTo = req.query.dateTo;
    let options = {
      attributes: ["humidity", "createdAt"],
      where: {
        refDeviceId: deviceId,
        createdAt: {
          [Op.gte]: dateFrom,
          [Op.lte]: dateTo,
        },
      },
      order: [["createdAt", "ASC"]],
    };
    try {
      Data.findAll(options).then((humByInterval) => {
        res.send(humByInterval);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getCo2ByInterval(req, res) {
    const deviceId = req.query.selectedDevices;
    const dateFrom = req.query.dateFrom;
    const dateTo = req.query.dateTo;
    let options = {
      attributes: ["eco2", "createdAt"],
      where: {
        refDeviceId: deviceId,
        createdAt: {
          [Op.gte]: dateFrom,
          [Op.lte]: dateTo,
        },
      },
      order: [["createdAt", "ASC"]],
    };
    try {
      Data.findAll(options).then((co2ByInterval) => {
        res.send(co2ByInterval);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getLuxByInterval(req, res) {
    const deviceId = req.query.selectedDevices;
    const dateFrom = req.query.dateFrom;
    const dateTo = req.query.dateTo;
    let options = {
      attributes: ["lux", "createdAt"],
      where: {
        refDeviceId: deviceId,
        createdAt: {
          [Op.gte]: dateFrom,
          [Op.lte]: dateTo,
        },
      },
      order: [["createdAt", "ASC"]],
    };
    try {
      Data.findAll(options).then((lightByInterval) => {
        res.send(lightByInterval);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getWifiByInterval(req, res) {
    const deviceId = req.query.selectedDevices;
    const dateFrom = req.query.dateFrom;
    const dateTo = req.query.dateTo;
    let options = {
      attributes: ["rssi", "createdAt"],
      where: {
        refDeviceId: deviceId,
        createdAt: {
          [Op.gte]: dateFrom,
          [Op.lte]: dateTo,
        },
      },
      order: [["createdAt", "ASC"]],
    };
    try {
      Data.findAll(options).then((wifiByInterval) => {
        res.send(wifiByInterval);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getAirByInterval(req, res) {
    const deviceId = req.query.selectedDevices;
    const dateFrom = req.query.dateFrom;
    const dateTo = req.query.dateTo;
    let options = {
      attributes: ["tvoc", "createdAt"],
      where: {
        refDeviceId: deviceId,
        createdAt: {
          [Op.gte]: dateFrom,
          [Op.lte]: dateTo,
        },
      },
      order: [["createdAt", "ASC"]],
    };
    try {
      Data.findAll(options).then((airByInterval) => {
        res.send(airByInterval);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getPressureByInterval(req, res) {
    const deviceId = req.query.selectedDevices;
    const dateFrom = req.query.dateFrom;
    const dateTo = req.query.dateTo;
    let options = {
      attributes: ["pressure", "createdAt"],
      where: {
        refDeviceId: deviceId,
        createdAt: {
          [Op.gte]: dateFrom,
          [Op.lte]: dateTo,
        },
      },
      order: [["createdAt", "ASC"]],
    };
    try {
      Data.findAll(options).then((airByInterval) => {
        res.send(airByInterval);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getDeviceCountByInterval(req, res) {
    const deviceId = req.query.selectedDevices;
    const dateFrom = req.query.dateFrom;
    const dateTo = req.query.dateTo;
    let options = {
      attributes: ["deviceCount", "createdAt"],
      where: {
        refDeviceId: deviceId,
        createdAt: {
          [Op.gte]: dateFrom,
          [Op.lte]: dateTo,
        },
      },
      order: [["createdAt", "ASC"]],
    };
    try {
      Data.findAll(options).then((airByInterval) => {
        res.send(airByInterval);
      });
    } catch (err) {
      console.log(err);
    }
  },
};
