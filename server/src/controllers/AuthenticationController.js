const {
  User,
  Group,
  Device,
  Role,
  PackageService,
  PaidService,
} = require("../models");
const jwt = require("jsonwebtoken");
const config = require("../config/config");

function jwtSignUser(user) {
  const ONE_DAY = 60 * 60 * 24 * 1;
  return jwt.sign(user, config.authentication.jwtSecret, {
    expiresIn: ONE_DAY,
  });
}

module.exports = {
  async register(req, res) {
    try {
      const group = await Group.create({
        name: req.body.email,
      });
      const refGroup = group.id;

      const user = await User.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        refRole: 1,
        refGroup: refGroup,
      });

      const userJson = user.toJSON();
      const groupJson = group.toJSON();
      res.send({
        user: userJson,
        group: groupJson,
        token: jwtSignUser(userJson),
      });
    } catch (err) {
      console.log(err);
      res.status(400).send({
        error: "This email account is already in use",
      });
    }
  },

  // Register method for users belonging to a company
  // Company modules are already pre-set, therefore setupDevice page is not available
  async registerCompanyUser(req, res) {
    try {
      const companyId = await Group.findOne({
        raw: true,
        nest: true,
        where: {
          name: req.body.companyId,
        },
      });
      if (companyId === null) {
        return res.status(403).send({
          error: "Company ID does not exist.",
        });
      }

      const user = await User.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        refRole: 1,
        refGroup: companyId.id,
      });

      const userJson = user.toJSON();
      res.send({
        user: userJson,
        token: jwtSignUser(userJson),
      });
    } catch (err) {
      console.log(err);
      res.status(400).send({
        error: "This email account is already in use",
      });
    }
  },

  async setupDevice(req, res) {
    try {
      console.log(req.body);
      const device = await Device.findOne({
        where: {
          mac: req.body.mac,
        },
      });
      const devicePackage = device.refPackage;
      console.log('device.rssiThreshold: ', device.rssiThreshold)
      if(device.rssiThreshold === null) {
        device.rssiThreshold = -90;
      }
      console.log('device.rssiThreshold AFTER: ', device.rssiThreshold)

      const user = await User.findOne({
        where: {
          email: req.body.email,
        },
      });
      if (user === null) {
        return res.status(403).send({
          error: "Login credentials incorrect.",
        });
      }
      let isPasswordValid = await user.comparePassword(req.body.password);
      if (!isPasswordValid) {
        return res.status(403).send({
          error: "Login credentials incorrect.",
        });
      }

      const group = await Group.findOne({
        where: {
          id: user.refGroup,
        },
      });
      group.refPackage = devicePackage;
      device.refGroup = group.id;

      if (device.name === null) {
        device.name = req.body.nickname;
      }
      await group.save();
      await device.save();

      res.send({
        messageType: "success",
        mqttip: "94.46.178.132",
        mqttport: "9678",
        mqttusr: "sensorblock",
        mqttpwd: "mR#2$cCpKcWDunv*",
      });
    } catch (err) {
      console.log("ERROR:", err);
      res.status(500).send({
        error: "An error occurred while trying to setup the device.",
      });
    }
  },

  async login(req, res) {
    try {
      let { email, password } = req.body;
      let user = await User.findOne({
        where: {
          email: email,
        },
      });
      if (user === null) {
        return res.status(403).send({
          error: "Login credentials incorrect.",
        });
      }
      let isPasswordValid = await user.comparePassword(password);
      if (!isPasswordValid) {
        return res.status(403).send({
          error: "Login credentials incorrect.",
        });
      }
      let devices = await Device.findAll({
        where: {
          refGroup: user.refGroup,
        },
        order: [["id", "ASC"]],
      });
      let role = await Role.findOne({
        where: {
          id: user.refRole,
        },
      });
      let group = await Group.findOne({
        where: {
          id: user.refGroup,
        },
      });
      let userServices = await PackageService.findAll({
        where: {
          refPackage: group.refPackage,
        },
      });

      let userExtras = await PaidService.findAll({
        where: {
          refGroup: group.id,
        },
      });

      let userJson = user.toJSON();
      let groupJson = group.toJSON();
      res.send({
        user: userJson,
        group: groupJson,
        role: role.value,
        devices: devices,
        services: userServices,
        extras: userExtras,
        token: jwtSignUser(userJson),
      });
    } catch (err) {
      console.log(err);
      res.status(500).send({
        error: "An error occurred while trying to login.",
      });
    }
  },

  async setupTest(req, res) {
    console.log("req.body", req.body);
    try {
      res.send({
        messageType: "success",
        mqttip: "94.46.178.132",
        mqttport: "9678",
        mqttusr: "sensorblock",
        mqttpwd: "mR#2$cCpKcWDunv*",
      });
    } catch (err) {
      res.status(500).send({
        error: "Test Error.",
      });
      console.log("response:", res);
    }
  },
};
