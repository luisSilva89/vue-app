const { Device } = require("../models");

module.exports = {
  async addDevice(req, res) {
    console.log('REQ BODY:', req.body)
    let options = {
      serialNumber: req.body.serialNumber,
      mac: req.body.mac,
      refPackage: req.body.refPackage,
    };
    try {
      const device = await Device.create(options);
      const deviceJson = device.toJSON();
      res.send(deviceJson);
    } catch (err) {
      console.log(err)
      res.status(400).send({
        error: "Add device failed, check serial number and mac input.",
      });
    }
  },

  async updateDevice(req, res) {
    let options = {
      where: {
        serialNumber: req.body.serialNumber,
      },
    };
    try {
      const device = await Device.findOne(options);
      device.refPackage = req.body.refPackage;
      await device.save();
      let deviceJson = device.toJSON();
      res.send({
        device: deviceJson,
      });
    } catch (err) {
      res.status(400).send({
        error: "Device not found, check serial number input.",
      });
    }
    console.log('EXITING CONTROLLER')
  },

  async getGroupDevices(req, res) {
    const groupId = Number(req.query.groupId);
    options = {
      where: {
        refGroup: groupId,
      },
    };
    try {
      Device.findAll(options).then((devices) => {
        res.send(devices);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getAllDevices(req, res) {
    try {
      let devices = await Device.findAll();
      res.send(devices);
    } catch (err) {
      console.log(err);
    }
  },

  async delete(req, res) {
    const id = Number(req.params.id);

    let options = {
      where: {
        id: id,
      },
    };
    try {
      await Device.destroy(options);
      res.status(200).send();
    } catch (err) {
      console.log(err);
    }
  },

  async updateDeviceInfo(req, res) {
    let options = {
      where: {
        id: req.body.id,
      },
    };
    try {
      let device = await Device.findOne(options);
      console.log("device", device);
      device.name = req.body.name;
      device.installationLocal = req.body.installLocal;
      await device.save();

      let deviceJson = device.toJSON();
      res.send({
        device: deviceJson,
      });
    } catch (err) {
      res.status(500).send({
        error: "An error occurred while updating device info.",
      });
      console.log("ERROR:", err);
    }
  },
};
