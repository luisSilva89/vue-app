const { Group, User } = require("../models");

module.exports = {
  async getGroupUsers(req, res) {
    // the user object comes from the authentication done by the passport library
    const groupId = req.user.refGroup;
    options = {
      where: {
        refGroup: groupId,
      },
    };
    try {
      User.findAll(options).then((groupUsers) => {
        res.send(groupUsers);
      });
    } catch (err) {
      console.log(err);
    }
  },

  async getAllGroups(req, res) {
    try {
      Group.findAll().then((groups) => {
        res.send(groups);
      });
    } catch (err) {
      console.log(err);
    }
  },
};
