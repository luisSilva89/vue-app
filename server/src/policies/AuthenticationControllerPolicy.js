// Joi framework will do input validation for us
const Joi = require("joi");

module.exports = {
  register(req, res, next) {
    const schema = Joi.object({
      email: Joi.string().email(),
      password: Joi.string().regex(new RegExp("^[a-zA-Z0-9]{8,32}$")),
      username: Joi.any(),
      accessCode: Joi.number(),
    });

    const { error } = schema.validate(req.body);

    if (error) {
      switch (error.details[0].context.key) {
        case "email":
          res.status(400).send({
            error: "Email provided is not in a valid format",
          });
          break;
        case "password":
          res.status(400).send({
            error: `The password must match the following rules:
              <br>
              1. It must contain only the following characters: lower case, upper case, numerics.
              <br>
              2. It must be at least 8 characters length and no more than 32 characters.`,
          });
        default:
          res.status(400).send({
            error: "Invalid registration information. Try again.",
          });
      }
    } else {
      next();
    }
  },

  registerCompanyUser(req, res, next) {
    const schema = Joi.object({
      email: Joi.string().email(),
      password: Joi.string().regex(new RegExp("^[a-zA-Z0-9]{8,32}$")),
      username: Joi.any(),
      // TODO: Issue with companyId validation
      companyId: Joi.any().allow("#"),
    });

    const { error } = schema.validate(req.body);

    if (error) {
      switch (error.details[0].context.key) {
        case "email":
          res.status(400).send({
            error: "Email provided is not in a valid format",
          });
          break;
        case "password":
          res.status(400).send({
            error: `The password must match the following rules:
              <br>
              1. It must contain only the following characters: lower case, upper case, numerics.
              <br>
              2. It must be at least 8 characters length and no more than 32 characters.`,
          });
        default:
          res.status(400).send({
            error: "Invalid registration information. Try again.",
          });
      }
    } else {
      next();
    }
  },

  createUser(req, res, next) {
    const schema = Joi.object({
      email: Joi.string().email(),
      password: Joi.string().regex(new RegExp("^[a-zA-Z0-9]{8,32}$")),
      username: Joi.string(),
      refRole: Joi.string(),
      refGroup: Joi.number(),
    });

    const { error } = schema.validate(req.body);

    if (error) {
      console.log("ERROR: ", error);
      switch (error.details[0].context.key) {
        case "email":
          res.status(400).send({
            error: "Email provided is not in a valid format",
          });
          break;
        case "username":
          res.status(400).send({
            error: "Username provided must be in a string format",
          });
          break;
        case "password":
          res.status(400).send({
            error: `The password must match the following rules:
              <br>
              1. It must contain only the following characters: lower case, upper case, numerics.
              <br>
              2. It must be at least 8 characters length and no more than 32 characters.`,
          });
        default:
          res.status(400).send({
            error: "Invalid registration information. Try again.",
          });
      }
    } else {
      next();
    }
  },
};
