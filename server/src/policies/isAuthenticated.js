const passport = require("passport");

module.exports = function (req, res, next) {
  // below we call the authenticate method from passport.js and we pass it a 'jwt' strategy that we have defined
  passport.authenticate("jwt", function (err, user) {
    if (err || !user) {
      res.status(403).send({
        error: "access not permitted",
      });
    } else {
      req.user = user;
      next();
    }
  })(req, res, next);
};
