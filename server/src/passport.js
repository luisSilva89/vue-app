// including module passport from the passport library
const passport = require("passport");
const { User } = require("./models");

const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const config = require("./config/config");

passport.use(
  new JwtStrategy(
    {
      // options we will be using on this strategy
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.authentication.jwtSecret,
    },
    // below we define the strategy
    async function (jwtPayload, done) {
      try {
        const user = await User.findOne({
          where: {
            id: jwtPayload.id,
          },
        });
        if (!user) {
          return done(new Error(), false);
        }
        return done(null, user);
      } catch (err) {
        return done(new Error(), false);
      }
    }
  )
);

// in this file we are not exporting anything, we are just setting up the passport object that we will be using
module.exports = null;
