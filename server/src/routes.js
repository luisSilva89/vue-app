const AuthenticationController = require("./controllers/AuthenticationController");
const AuthenticationControllerPolicy = require("./policies/AuthenticationControllerPolicy");
const DataController = require("./controllers/DataController");
const DeviceController = require("./controllers/DeviceController");
const UserController = require("./controllers/UserController");
const GroupController = require("./controllers/GroupController");
const EventController = require("./controllers/EventController");

const isAuthenticated = require("./policies/isAuthenticated");

module.exports = (app) => {
  // AUTHENTICATION CONTROLLER
  app.post(
    "/register",
    AuthenticationControllerPolicy.register,
    AuthenticationController.register
  );

  app.post(
    "/registerCompany",
    AuthenticationControllerPolicy.registerCompanyUser,
    AuthenticationController.registerCompanyUser
  );

  app.post("/login", AuthenticationController.login);

  app.post("/setupDevice", AuthenticationController.setupDevice);
  
  app.post("/setupTest", AuthenticationController.setupTest);

  // USER CONTROLLER
  app.post("/updateUserInfo", isAuthenticated, UserController.updateUserInfo);

  app.get("/getUserInfo", isAuthenticated, UserController.getUserInfo);

  app.post(
    "/createUser",
    isAuthenticated,
    AuthenticationControllerPolicy.createUser,
    UserController.createUser
  );

  app.get("/getAllUsers", isAuthenticated, UserController.getAllUsers);

  app.delete("/user/:id", isAuthenticated, UserController.delete);

  // DEVICE CONTROLLER
  app.post("/addDevice", isAuthenticated, DeviceController.addDevice);
  
  app.post("/updateDevice", isAuthenticated, DeviceController.updateDevice);

  app.get("/getGroupDevices", isAuthenticated, DeviceController.getGroupDevices);

  app.get("/getAllDevices", isAuthenticated, DeviceController.getAllDevices);

  app.delete("/device/:id", isAuthenticated, DeviceController.delete);

  app.post("/updateDeviceInfo", isAuthenticated, DeviceController.updateDeviceInfo)

  // GROUP CONTROLLER
  app.get("/getGroupUsers", isAuthenticated, GroupController.getGroupUsers);

  app.get("/getAllGroups", isAuthenticated, GroupController.getAllGroups);

  // ALERT CONTROLLER
  app.post("/createNewEntry", isAuthenticated, EventController.createNewEntry);

  app.get("/getEntries", isAuthenticated, EventController.getEntries);

  app.post("/updateAction", isAuthenticated, EventController.updateAction);

  app.get(
    "/getCurrentEvents",
    isAuthenticated,
    EventController.getCurrentEvents
  );

  app.get("/getEvents", isAuthenticated, EventController.getEvents);

  app.delete("/entry/:id", isAuthenticated, EventController.delete);

  // DATA CONTROLLER
  app.post("/sensors", isAuthenticated, DataController.getCurrentData);

  app.get(
    "/sensors/defaultData",
    isAuthenticated,
    DataController.getDefaultPlotData
  );

  app.get(
    "/sensors/dataFromAllDevices",
    isAuthenticated,
    DataController.getDataFromAllDevices
  );

  app.get(
    "/sensors/dataByInterval",
    isAuthenticated,
    DataController.getDataByInterval
  );

  app.get(
    "/sensors/temperature",
    isAuthenticated,
    DataController.getTempByInterval
  );

  app.get("/sensors/noise", isAuthenticated, DataController.getNoiseByInterval);

  app.get(
    "/sensors/humidity",
    isAuthenticated,
    DataController.getHumByInterval
  );

  app.get("/sensors/co2", isAuthenticated, DataController.getCo2ByInterval);
  app.get("/sensors/ldr", isAuthenticated, DataController.getLuxByInterval);
  app.get("/sensors/wifi", isAuthenticated, DataController.getWifiByInterval);
  app.get("/sensors/air", isAuthenticated, DataController.getAirByInterval);
  app.get("/sensors/pressure", isAuthenticated, DataController.getPressureByInterval);
  app.get("/sensors/deviceCount", isAuthenticated, DataController.getDeviceCountByInterval);
};
