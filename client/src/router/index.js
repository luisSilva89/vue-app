import Vue from "vue";
import Router from "vue-router";
import HomePage from "@/pages/userPages/HomePage";
import Register from "@/pages/userPages/Register";
import RegisterCompany from "@/pages/userPages/RegisterCompany";
import Login from "@/pages/userPages/Login";
import LobbyPage from "@/pages/userPages/LobbyPage";
import PersonalInfo from "@/pages/userPages/PersonalInfo";
import DeviceSetup from "@/pages/userPages/DeviceSetup";
import Profile from "@/pages/userPages/Profile";
import Control from "@/pages/userPages/Control";
import Analytics from "@/pages/userPages/Analytics";
import Devices from "@/pages/userPages/Devices";
import Users from "@/pages/userPages/Users";
import AppUsers from "@/pages/masterPages/AppUsers";
import AppDevices from "@/pages/masterPages/AppDevices";
import DataView from "@/pages/userPages/DataView";
import EventLog from "@/pages/userPages/EventLog";
import store from "@/store/store";

Vue.use(Router);

const myRouter = new Router({
  routes: [
    {
      path: "/",
      name: "root",
      component: HomePage,
    },
    {
      path: "/register",
      name: "register",
      component: Register,
    },
    {
      path: "/registerCompany",
      name: "registerCompany",
      component: RegisterCompany,
    },
    {
      path: "/deviceSetup",
      name: "deviceSetup",
      component: DeviceSetup,
    },
    {
      path: "/login",
      name: "login",
      component: Login,
    },
    {
      path: "/personalInfo",
      name: "personalInfo",
      component: PersonalInfo,
      // Navigation Guard
      beforeEnter: (to, from, next) => {
        if (store.state.isUserLoggedIn) {
          next();
        } else {
          next("/login");
        }
      },
    },
    {
      path: "/lobby",
      name: "lobby",
      component: LobbyPage,
      beforeEnter: (to, from, next) => {
        if (store.state.isUserLoggedIn) {
          next();
        } else {
          next("/login");
        }
      },
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile,
      beforeEnter: (to, from, next) => {
        if (store.state.isUserLoggedIn) {
          next();
        } else {
          next("/login");
        }
      },
    },
    {
      path: "/eventLog",
      name: "eventLog",
      component: EventLog,
      beforeEnter: (to, from, next) => {
        if (store.state.isUserLoggedIn) {
          next();
        } else {
          next("/login");
        }
      },
    },
    {
      path: "/control",
      name: "control",
      component: Control,
      beforeEnter: (to, from, next) => {
        if (store.state.isUserLoggedIn) {
          next();
        } else {
          next("/login");
        }
      },
    },
    {
      path: "/analytics",
      name: "analytics",
      component: Analytics,
      beforeEnter: (to, from, next) => {
        if (store.state.isUserLoggedIn) {
          next();
        } else {
          next("/login");
        }
      },
    },
    {
      path: "/devices",
      name: "devices",
      component: Devices,
      beforeEnter: (to, from, next) => {
        if (store.state.isUserLoggedIn) {
          next();
        } else {
          next("/login");
        }
      },
    },
    {
      path: "/appDevices",
      name: "AppDevices",
      component: AppDevices,
      beforeEnter: (to, from, next) => {
        if (store.state.isUserLoggedIn && store.state.userRole === "master") {
          next();
        } else {
          next("/");
        }
      },
    },
    {
      path: "/users",
      name: "users",
      component: Users,
      beforeEnter: (to, from, next) => {
        if (store.state.isUserLoggedIn) {
          next();
        } else {
          next("/login");
        }
      },
    },
    {
      path: "/appUsers",
      name: "AppUsers",
      component: AppUsers,
      beforeEnter: (to, from, next) => {
        if (store.state.isUserLoggedIn && store.state.userRole === "master") {
          next();
        } else {
          next("/");
        }
      },
    },
    {
      path: "/dataView",
      name: "DataView",
      component: DataView,
    },
    {
      path: "*",
      redirect: "/",
    },
  ],
});

export default myRouter;
