import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { sync } from "vuex-router-sync";
import vuetify from "@/plugins/vuetify";
import store from "@/store/store";
//TODO Import Echarts locally
import ECharts from "vue-echarts";
import moment from "moment";
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.prototype.moment = moment
Vue.config.productionTip = false;

Vue.use(ECharts);

sync(store, router);

new Vue({
  render: (h) => h(App),
  vuetify,
  router,
  store,
}).$mount("#app");
