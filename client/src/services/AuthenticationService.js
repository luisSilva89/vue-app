import Api from "@/services/Api";

export default {

  // Creates a new entry on the "Users" table. Takes an object (credentials).
  register(credentials) {
    return Api().post("register", credentials);
  },

  // Creates a new entry on the "Users" table. Takes an object (credentials).
  registerCompany(credentials) {
    return Api().post("registerCompany", credentials);
  },

  login(credentials) {
    return Api().post("login", credentials);
  },
};
