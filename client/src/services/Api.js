import axios from "axios";
import store from '@/store/store'


export default () => {
  return axios.create({
    baseURL: "http://localhost:8081/",
    headers: {
      //When registered or loged in the user will be given a token, this token will be placed on the header for any
      //request to the API by the below code. This way we guarantee the requester has permission to make the request.
      Authorization: `Bearer ${store.state.token}`
    }
  });
};
