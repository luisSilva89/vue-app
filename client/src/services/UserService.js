import Api from "@/services/Api";

export default {
  getUserInfo(id) {
    return Api().get("getUserInfo", id);
  },
  updateUserInfo(info) {
    return Api().post("updateUserInfo", info);
  },
  createUser(credentials) {
    return Api().post("createUser", credentials);
  },
  getAllUsers() {
    return Api().get("getAllUsers")
  },
  delete(id) {
    return Api().delete(`/user/${id}`);
    // return Api().delete('/entry/' + id);
  },
};
