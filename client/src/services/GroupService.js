import Api from "@/services/Api";

export default {
  getGroupInfo(id) {
    return Api().get("getGroupInfo", id);
  },
  updateGroupInfo(info) {
    return Api().post("updateGroupInfo", info);
  },
  getGroupUsers(refGroup) {
    return Api().get("getGroupUsers", {
      params: { refGroup },
    });
  },
  getAllGroups() {
    return Api().get("getAllGroups")
  }
};
