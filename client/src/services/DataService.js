import Api from "@/services/Api";

export default {
  getCurrentData(device) {
    return Api().post("sensors", {
      params: {
        device,
      },
    });
  },

  getDefaultPlotData(deviceId, dateFrom, dateTo) {
    return Api().get("sensors/defaultData", {
      params: {
        deviceId,
        dateFrom,
        dateTo,
      },
    });
  },

  getDataFromAllDevices(deviceIds, dataType) {
    return Api().get("sensors/dataFromAllDevices", {
      params: {
        deviceIds,
        dataType,
      },
    });
  },

  getDataByInterval(selectedDevices, dateFrom, dateTo) {
    return Api().get("sensors/dataByInterval", {
      params: {
        selectedDevices,
        dateFrom,
        dateTo,
      },
    });
  },

  getDataOfType(selectedDevices, dateFrom, dateTo, dataOfType) {
    switch (dataOfType) {
      case "Temperature":
        return this.getTempByInterval(selectedDevices, dateFrom, dateTo);
      case "Humidity":
        return this.getHumByInterval(selectedDevices, dateFrom, dateTo);
      case "Co2":
        return this.getCo2ByInterval(selectedDevices, dateFrom, dateTo);
      case "Noise":
        return this.getNoiseByInterval(selectedDevices, dateFrom, dateTo);
      case "Light":
        return this.getLuxByInterval(selectedDevices, dateFrom, dateTo);
      case "Wifi":
        return this.getWifiByInterval(selectedDevices, dateFrom, dateTo);
      case "Air":
        return this.getAirByInterval(selectedDevices, dateFrom, dateTo);
      case "Pressure":
        return this.getPressureByInterval(selectedDevices, dateFrom, dateTo);
      case "Number Of Devices":
        return this.getDeviceCountByInterval(selectedDevices, dateFrom, dateTo);
    }
  },

  getPressureByInterval(selectedDevices, dateFrom, dateTo) {
    return Api().get("sensors/pressure", {
      params: {
        selectedDevices,
        dateFrom,
        dateTo,
      },
    });
  },

  getDeviceCountByInterval(selectedDevices, dateFrom, dateTo) {
    return Api().get("sensors/deviceCount", {
      params: {
        selectedDevices,
        dateFrom,
        dateTo,
      },
    });
  },

  getAirByInterval(selectedDevices, dateFrom, dateTo) {
    return Api().get("sensors/air", {
      params: {
        selectedDevices,
        dateFrom,
        dateTo,
      },
    });
  },

  getWifiByInterval(selectedDevices, dateFrom, dateTo) {
    console.log("2");
    return Api().get("sensors/wifi", {
      params: {
        selectedDevices,
        dateFrom,
        dateTo,
      },
    });
  },

  getTempByInterval(selectedDevices, dateFrom, dateTo) {
    return Api().get("sensors/temperature", {
      params: {
        selectedDevices,
        dateFrom,
        dateTo,
      },
    });
  },

  getNoiseByInterval(selectedDevices, dateFrom, dateTo) {
    return Api().get("sensors/noise", {
      params: {
        selectedDevices,
        dateFrom,
        dateTo,
      },
    });
  },

  getHumByInterval(selectedDevices, dateFrom, dateTo) {
    return Api().get("sensors/humidity", {
      params: {
        selectedDevices,
        dateFrom,
        dateTo,
      },
    });
  },

  getCo2ByInterval(selectedDevices, dateFrom, dateTo) {
    return Api().get("sensors/co2", {
      params: {
        selectedDevices,
        dateFrom,
        dateTo,
      },
    });
  },

  getLuxByInterval(selectedDevices, dateFrom, dateTo) {
    return Api().get("sensors/ldr", {
      params: {
        selectedDevices,
        dateFrom,
        dateTo,
      },
    });
  },
};
