import Api from "@/services/Api";

export default {

  addDevice(info) {
    return Api().post("addDevice", info);
  },
  updateDevice(info) {
    return Api().post("updateDevice", info);
  },
  // Returns all devices for a given group
  getGroupDevices(groupId) {
    return Api().get("getGroupDevices", {
      params: { groupId },
    });
  },
  getAllDevices() {
    return Api().get("getAllDevices");
  },
  delete(id) {
    return Api().delete(`/device/${id}`);
  },
  updateDeviceInfo(info) {
    return Api().post("updateDeviceInfo", info)
  }
};
